import os
import io
from string import Template

path = os.getcwd()
print ("Current directory is: %s" % path)

#Create empty list and set up directories
modelList = []
templatedir = "/mnt/c/Users/Steam/ros"
parentdir = "rosrider_arena"

def main():

    #Input loop
    while True:
        tempModel = getModelName()

        if tempModel.startswith("range_"):
            numModel = getnumModel()

            for i in range(1,numModel+1):
                newtempModel = removePrefix(tempModel, "range_")
                newtempModel = newtempModel + "_" + str(i)
                modelList.append(newtempModel)

        elif tempModel != "done":
            modelList.append(tempModel)

        else:
            break

    #Main loop
    for i in range(0,len(modelList)):

        modelPath = str(path) + "/" + modelList[i]
        os.makedirs(modelPath)
        os.chdir(modelPath)
        os.mkdir("meshes")

        templatedirSDF = str(templatedir) + "/" + "tempSDF.txt"
        tempSDF = io.open(templatedirSDF, "r")
        emptySDF = io.open("model.sdf", "w")
        lineReplacer(tempSDF, emptySDF, modelList[i])

        templatedirCONFIG = str(templatedir) + "/" + "tempCONFIG.txt"
        tempCONFIG = open(templatedirCONFIG, "r")
        emptyCONFIG = io.open("model.config", "w")
        lineReplacer(tempCONFIG, emptyCONFIG, modelList[i])
        os.chdir(path)

def getModelName():
    modelName = raw_input("Enter a model name: ")
    return modelName

def getnumModel():
    numModel = int(raw_input("Enter the number of models: "))
    return numModel

def removePrefix(modelname, prefix):
    resName = modelname.replace(prefix, "", 1)
    return resName

def lineReplacer(tempFile, emptyFile, currentModel):
    for line in tempFile:
        line = line.replace("$modelName", currentModel)
        line = line.replace("$parent_directory", parentdir)
        emptyFile.write(line.decode('utf-8'))
    emptyFile.close()

if __name__ == "__main__":
    main()
